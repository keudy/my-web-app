FROM registry.redhat.io/jboss-webserver-5/webserver52-openjdk8-tomcat9-openshift-rhel7:1.0

RUN mkdir /opt/prometheus
COPY jmx_prometheus_javaagent-0.12.0.jar /opt/prometheus
COPY target/my-web-app.war /deployments
