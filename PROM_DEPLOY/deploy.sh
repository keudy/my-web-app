oc new-project my-prometheus

# Create the prom secret
oc create secret generic prom --from-file=prometheus.yml

# Create the prom-alerts secret
oc create secret generic prom-alerts --from-file=alertmanager.yml

# Create the prometheus instance
oc process -f prometheus-standalone.yaml | oc apply -f -

oc adm policy add-cluster-role-to-user view -z prom

oc process -f grafana.yaml -p NAMESPACE=my-prometheus | oc apply -f -
